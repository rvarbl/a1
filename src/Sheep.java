public class Sheep {

    enum Animal {sheep, goat}

    public static void main(String[] param) {
        // for debugging
        Animal[] animals = {Animal.goat, Animal.sheep, Animal.goat, Animal.sheep, Animal.sheep};
//        Animal[] animals = null;
        for (Animal animal : animals) {
            System.out.println(animal);
        }
        System.out.println("-----");
        Sheep.reorder(animals);
        for (Animal animal : animals) {
            System.out.println(animal);
        }

    }

    public static void reorder(Animal[] animals) {

        if (animals == null) {
            throw new RuntimeException("Input is null!");
        }
        if (animals.length < 2) {
            return;
        }

        int goatCount = 0;
        for (int i = 0; i < animals.length; i++) {
            if (animals[i].equals(Animal.goat)) {
                goatCount += 1;
            }
        }

        if (goatCount == 0 || animals.length == goatCount) {
            return;
        }

        for (int i = 0; i < goatCount; i++) {
            animals[i] = Animal.goat;
        }
        for (int i = goatCount; i < animals.length; i++) {
            animals[i] = Animal.sheep;
        }

    }
}

